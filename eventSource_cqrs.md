Event Souce e CQRS
---

Referencias: 

- https://movile.blog/cqrs-event-sourcing-vantagens-de-uma-arquitetura-orientada-a-eventos/ (1)
- https://medium.com/codigorefinado/cqrs-command-query-responsability-segregation-98848c274233 (2)
- https://www.revista-programar.info/artigos/o-que-esconde-o-cqrs/ (3)
- https://pt.stackoverflow.com/questions/181688/o-que-%C3%A9-cqrs-e-como-implementar (4)
- https://docs.microsoft.com/en-us/previous-versions/msp-n-p/dn568103(v=pandp.10)?redirectedfrom=MSDN (5)
- https://docs.microsoft.com/en-us/previous-versions/msp-n-p/dn589792%28v%3dpandp.10%29 (6)

---

Backlog:
- https://docs.microsoft.com/en-us/previous-versions/msp-n-p/dn589795%28v%3dpandp.10%29
- https://docs.microsoft.com/en-us/previous-versions/msp-n-p/jj591577%28v%3dpandp.10%29
- https://docs.microsoft.com/en-us/previous-versions/msp-n-p/jj554200%28v%3dpandp.10%29
- https://docs.microsoft.com/en-us/previous-versions/msp-n-p/dn589800%28v%3dpandp.10%29
- https://www.martinfowler.com/bliki/CQRS.html
- https://docs.microsoft.com/pt-br/archive/msdn-magazine/2015/june/cutting-edge-cqrs-for-the-common-application
- https://www.youtube.com/watch?v=G5kuemKALgw
- https://en.wikipedia.org/wiki/Command%E2%80%93query_separation
- https://microservices.io/patterns/data/cqrs.html?source=post_page---------------------------
- https://martinfowler.com/bliki/CQRS.html?source=post_page---------------------------
- https://martinfowler.com/eaaDev/EventSourcing.html?source=post_page---------------------------
- https://cqrs.files.wordpress.com/2010/11/cqrs_documents.pdf
- https://en.wikipedia.org/wiki/Eventual_consistency
- https://martinfowler.com/eaaDev/EventSourcing.html

---

## INTRO

(1)
- Event Sourcing favorece - Imutabilidade e Auditoria
- Priorizar as acoes e eventos dos sistemas sobre a modelagem
- CQRS e Event Sourcing sao conceitoa independentes, mas que podem ser implementados em conjunto em uma boa aplicacao de orientacao a eventos

(2)
- Requer responsabilidade de identificar que realmente precisa-se adotar a solucao mais complexa para resolver um problema nao trivial
- DDD e RestFul sao conceitos conflitantes, um focado no dominio e outro em dados e exigem camadas de abstracao, transformacao e anticorrupcao
- CQRS e comumente apresentado em conjunto a Event Sourcing ou o NServiceBus, mas nao e obrigarorio que devemos utilizá-los, pois, isso traz junto complexidade que pode ser evitada em muitos casos

(5)
- Aplicativos altamente escalonaveis, disponiveis, e facil manutencao
- Um aproach (abordagem) tipico para tratar a consistencia eventual do CQRS e o uso de Event Sourcing para atualizar as visualizacoes do modelo de leitura

---

## CQRS pattern (Command Query Responsibility Segregation)

(1)

- E um padrao de microsservicos
- Em essencia CQRS é utilizar modelos diferentes para atualizar e ler informações
- A medida que os sistemas se tornam mais completos, tambem se tornam mais complexos, e comeca a ter varios usos para o mesmo dado em "representacoes" diferentes
- CQRS propoe a separacao de modelos diferentes para escrita (command) e leitura (query), ou seja objetos diferentes em processos separados

(2)
- Podemos ou ate devemos ter um modo diferente para consulta dos dados
- Isso se da, pois um sistema ha de 10 a 15 vezes mais leituras que gravacoes, e por isto e correto ter um modelo otimizado para consulta
- Faz sentido que podemos ter um banco de dados para manter os dados integros e outro otimizado para consultas
- Mas o conceito nao restringe que devemos ter bancos de dados segmentados, sendo implementado na camada de aplicacao e nao de persistencia
- De certa forma vai de encontro com alguns conceitos de (ACID), pois, admite a inconsistencia eventual, presando por disponibilidade e tolerancia, mas tudo isso quando a necessidade aparece, nao e uma regra
- CQRS e um padrao necessario principalemente quando o banco de dados e um gargalo para a escalabidade da aplicacao ou do negocio
- Se enquada muito bem para integracao entre aplicacoes
- Escalabilidade para atender milhares de requisicoes
- Quando se aplica modelo de persistencia separado do modelo de consultas (DTO e GraphQl casam muito bem com CQRS) 
- Traz aumento da complexidade

(3)
- CQRS parte de um princípio simples e que é capaz de evoluir para dar resposta a problemas mais complexos
- Origem baseada em CQS (Command-Query separation) que afirma que fazer uma pergunta nao deve alterar a sua resposta, ou seja, apenas deve apresentar os dados
- Em suma, CQRS é simplesmente isso, uma separação das duas responsabilidades
- Assim surgiram diversos reflexos e acoes no sistemas, como desnormalizacao de dados, cache, base de dados otimizadas para consultas
- Alguns beneficios (Capacidades do CQRS)
	- Consultas mais simples: mais simples e melhor desempenho
	- Bases de Dados separadas: Consulta a uma base otimizada para isto, mas nao algo obrigatorio
	- Fila de Trabalho: Comandos sendo processados como fila com processamento assincrono, gerindo assim processamentos complexos e carga no sistema
	- Separacao de carga: Como o sistema tem muito mais leituras que escritas, por isso questionamos se precisam seguir o mesmo padrao	
- Alguns maleficios (Desafios)
	- Complexidade
	- Mudanca de mindset, nao formatado ao CRUD, levando para tarefas e comandos, mudando os paradigmas, padroes e abstracoes

(4)
- CQRS é a separação da leitura e escrita dos dados em dois models distintos
- quando a necessidade de leitura dos dados requer um design muito diferente das necessidades de escrita destes dados
- Quando a leitura e escrita não exigem um design diferente mas você apenas precisa endereçar questões de performance ou de visões diferenciadas dos dados em algumas consultas, existem opções bem mais simples
- sendo model a definição de entidades e o relacionamento entre elas
	- Query model: responsável pela leitura dos dados do BD e pela atualização da interface gráfica
	- Command model: responsável pela escrita de dados no BD e pela validação dos dados. Sua interação com a interface gráfica é somente receber os dados a serem escritos.
- a segregação dos modelos diz respeito a separação física e/ou lógica dos dados
- sempre acarreta numa complexidade extra e por isso é necessário avaliar os cenários em que realmente são necessários trabalhar com este padrão.

(5)
- Maximizacao de desempenho, escalabilidade e seguranca, apoio a evolucao do sistema
- Em sistemas tradicionais, todas operacoes de CRUD sao aplicadas sobre a mesma representacao de modelo
	- há uma incompatibilidade entre as representações de leitura e gravação dos dados
		- como colunas ou propriedades adicionais que devem ser atualizadas corretamente, mesmo que não sejam necessárias como parte de uma operação.
	- Corre o risco de encontrar contenção de dados em um domínio colaborativo (onde vários atores operam em paralelo no mesmo conjunto de dados)
		- quando os registros são bloqueados no armazenamento de dados ou atualizam conflitos causados por atualizações simultâneas quando o bloqueio otimista é usado
		- Esses riscos aumentam à medida que a complexidade e a taxa de transferência do sistema aumentam
		- a abordagem tradicional também pode ter um efeito negativo no desempenho devido à carga no armazenamento de dados e na camada de acesso a dados
		- além da complexidade das consultas necessárias para recuperar informações
	- gerenciamento de segurança e permissões mais complicado
		- cada entidade está sujeita a operações de leitura e gravação, que podem expor inadvertidamente dados no contexto errado.
- Com CQRS temos a segregacao dos modelos de consulta e acoes
	-  simplifica consideravelmente o design e a implementação 
-  repositório de leitura pode ser uma réplica somente leitura do repositório de gravação ou os repositórios de leitura e gravação podem ter uma estrutura completamente diferente
- aumentar o desempenho e a segurança de um sistema, mas pode adicionar uma complexidade considerável em termos de resiliência e eventual consistência
- O armazenamento do modelo de leitura deve ser atualizado para refletir as alterações no armazenamento do modelo de gravação e pode ser difícil detectar quando um usuário emitiu uma solicitação com base em dados de leitura obsoletos (Consistencia eventual)
- Considere usar CQRS, quando for conveniente, nao como principio comum em todo sistema
- Um aproach (abordagem) tipico para tratar a consistencia eventual e o uso de Event Sourcing para atualizar as visualizacoes do modelo de leitura
- Quando usar
	- Domínios colaborativos em que várias operações são executadas em paralelo nos mesmos dados, O CQRS permite definir comandos com granularidade suficiente para minimizar conflitos de mesclagem no nível do domínio
	- Indicado para equipes já familiarizadas com as técnicas de DDD
	- Cenários em que o desempenho da leitura de dados deve ser ajustado separadamente do desempenho
	- Cenários em que o sistema deve evoluir ao longo do tempo e pode conter várias versões do modelo ou em que as regras de negócios mudam regularmente.
	- Integração com outros sistemas, especialmente em combinação com Event Sourcing, em que a falha temporal de um subsistema não deve afetar a disponibilidade dos outros.
- Esse padrão pode não ser adequado nas seguintes situações
	- Onde o domínio ou as regras de negócios são simples.
	- Onde uma interface de usuário simples do estilo CRUD e as operações de acesso a dados relacionadas são suficientes.
	- Para implementação em todo o sistema. Existem componentes específicos de um cenário geral de gerenciamento de dados em que o CQRS pode ser útil, mas pode adicionar uma complexidade considerável e muitas vezes desnecessária onde não é realmente necessário.



#### CAP Theorem
(2)
- Um sistema pode ser consistente, disponível e tolerante a particionamento (sharding ou replicação de dados), mas somente a 2 destes obrigatoriamente, o terceiro sera eventual
	- Se quero que o sistema seja consistente e disponível, eu não vou ter tolerância a particionamento.
	- Se quero que o sistema seja consistente e tenha tolerância a particionamento eu não vou ter ele sempre disponível
	- Se quero que tolerância a particionamento eu quero que os dados seja replicado a todas as bases, logo, mesmo que em um curto período de tempo terei estes dados indisponível
- Modelo de filas se encaixa muito bem com CQRS,  assim aceitamos as requisicoes e o processamento ocorre quando a disponibilidade de recursos, assumindo consistencia eventual ao inves de consistencia obrigatoria
- Trabalhar com esses dados sempre atualizados pode causar uma enorme perda de performance


#### Codigo de CQRS
##### Stack de Consulta 
(4)
- Preocupada com a recuperacao de dados
- Seu modelo de dados corresponde geralmente a camada de apresentacao (ou modelo mais amplo com graphql)
- Raramente possui regras de negocio, possui somente filtros e regras de autorização
- DDD é essencialmente uma forma de organizar a lógica do domínio
- representa o domínio de negócios do sistema, assim a API expressa a lógica principal do sistema.
(5)
- O modelo de leitura não possui lógica de negócios ou pilha de validação e apenas retorna um DTO para uso em um modelo de visualização. O modelo de leitura é eventualmente consistente com o modelo de gravação.

##### Stack de comando 
(4)
- Execução de tarefas que modificam o estado
- Implementacao de regras de negocio
- usar o CQRS não vincula você necessariamente ao DDD e coisas como agregações, fábricas e objetos de valor.
(5)
- O modelo de gravação possui uma pilha de processamento de comandos completa com lógica de negócios, validação de entrada e validação de negócios para garantir que tudo seja sempre consistente para cada um dos agregados

---

## Event Sourcing

(1)
- Em um modelo tradicional, se queremos saber o estado de um objeto, consultamos no banco de dados e temos esta informacao, mas isso nao nos mostra o que ocorreu com o dado para ele chegar neste estado.
- Event Sourcing garante que todas alteracoes sejam armazenadas em uma sequencia de eventos, possibilitando assim a projecao tanto do ultimo estado como consultar os eventos passados
- Por se ter o historico das alteracoes, temos natualmente a auditoria do negocio
- Uma arquitetura orientada a eventos aumenta a complexidade para manutencao e implementacao
- Um bom modelo de implementacao, implica em aplicar em um contexto menor antes de levar a um contexto maior e mais critico

(2) 
- Uma solucao quando a aplicacao exige historico, quando se precisa rever o um estado anterior e como chegou ao estado atual
- Um exemplo disto esta numa analise de eventos de extrato bancario para chegar em um valor consolidado ao final de um mes, onde se o processamento ocorrer a qualquer momento sera possivel chegar em uma consolidacao do valor total

(6)
- Use um armazenamento somente anexado para registrar a série completa de eventos que descrevem ações executadas nos dados em um domínio, em vez de armazenar apenas o estado atual
- Esse padrão pode simplificar tarefas em domínios complexos, evitando o requisito de sincronizar o modelo de dados e o domínio de negócios; melhorar o desempenho, escalabilidade e capacidade de resposta; fornecer consistência para dados transacionais; e manter trilhas e histórico de auditoria completos que possam permitir ações compensatórias.
- Uma abordagem tradicional implica em manter os dados atualizados usando de transacoes que bloqueiam os dados, trazendo algumas limitacoes:
	- As operacoes de atualizacao diretamente no armazenamento de dados prejudica o desempenho e limita a escalabilidade
	- Em um dominio colaborativo, com muitos usuarios e sistemas simultaneos, ocorrem conflitos de atualizacoes de dados
	- O historico e perdido a menos que se tenha um sistema adicional de auditoria e todo o custo que isso traz consigo
- Para resolver os problemas da abordagem tradicional, o Event Sourcing manipula os dados como uma sequencia de eventos
- Os eventos sao mantidos em um armazenamento de eventos que atua como fonte de verdade ou sistema de registro
- O armazenamento de eventos os publica para que os consumidores interessados possam consumir e manipular este dado
- Um uso tipico de eventos e de manter visualizacoes materializadas das entidades, como por exemplo, trabalhar como um canal que insere itens em uma lista de pedidos
- A Qualquer momento e possivel ler o historico de eventos e materializar o estado atual da entidade para que o objeto possa ser armazenado como uma visao materializada, para dar suporte a uma camada de aplicacao, armazenar o dado transformado em uma visao nova, analizado e consolidado....
- Algumas vantagens:
	- Eventos sao imutaveis, armazenados como operacoes somente de acrescimo
	- Eventos sao objetos simples que descrevem uma acao que ocorreu
	- Eventos nao atualizam diretamento o armazenamento de dados, simplesmente registram
	- Eventos tem um significado no contexto do dominio, que pode representar mais que simplemente o estado atual de um banco de dados tradicional	
	- Ajuda a impedir que atualizacoes simultaneas causem conflitos 
	- Armazenamento somente anexado fornece natualmente uma trilha de auditoria, usados para monitorar acoes, regerar o estado como visualizacoes e projecoes materializadas
	- A lista de eventos pode ser utilizada para detectar tendencias de comportamento dos dados, auxilinaod em insigts de negocio e de carga de uso dos sistemas 
	- A dissociação dos eventos de quaisquer tarefas que executam operações no sistema fornece flexibilidade e extensibilidade e simplicidade de codigo.
- Consideracoes
	- O sistema será eventualmente consistente ao criar vistas materializadas ou gerar projeções de dados ao reproduzir eventos: Ha um atraso na publicacao de um evento para o seu consumo e armazenamento
	- O Armazenamento e a fonte imutavel de informacoes, por isso os dados nunca devem ser alterados, a unica forma de desfazer ou alterar um evento e enviando um evento compensador
	- Para casos que o formato do dado do evento ser alterado, deve ser considerar o uso de metadado de versao do modelo
	- Em casos de multras threads ou instancias, a ordem dos eventos e vital para a manutencao da consistencia
	- Se os fluxos forem grandes, podem impactar na duracao do fluxo de eventos, considere utilizar "snapshots" em intervalos especificos 
	- A publicacao do evento pode ser "at least once" (pelo mensos uma vez), por isso os consumidores devem ser idempotentes, assim nao deve replicar a atualizacao descrita se o evento for tratado mais de uma vez
- Quando usar:
	- Para se capturar, “intent,” “purpose,” or “reason”
	- Para se evitar atualizacoes conflitantes nos dados
	- Se deseja gravar e reproduzir eventos para estrutuar e reproduzir estados
	- Para desacoplar o processo de entrada ou atualizacao de dados das tarefas que aplicam a acao: isso pode ocorrer para melhorar desemplenho da interface, distribuir eventos para outros listeners, 
	- Para ter flexibilidade para alterar o formato de modelos materizliados
	- Quando em conjunto com o CQRS, a consistencia eventual e aceitavel
- Nao pode ser usado:
	- Em sistemas que funcionem bem com os mecanismos simples de CRUD
	- Se a atualizacao de consistencia deve ser feita em tempo real
	- Se trilhas de auditorias e historico nao sao necessarias
	- Se nao ha ocorrencia de atualizacoes conflitantes


#### Event Sourcing e CQRS
(5)
- Ao usar o CQRS combinado com o padrão Event Sourcing, considere o seguinte:
	- Consistencia eventual 
- Quando usado CQRS com Event Sourcing, destacamos dois modelos de aplicacao:
	- Quando o armazenamento de eventos e o modelo de gravacao e e a fonte fonte autorizada de informações 
		- O uso do fluxo de eventos como armazenamento de gravação, em vez dos dados reais em um determinado momento, evita conflitos de atualização em um único agregado e maximiza o desempenho e a escalabilidade
		- Os eventos podem ser usados para gerar de forma assíncrona visualizações materializadas dos dados que são usados para preencher o armazenamento de leitura.
		- Como o armazenamento de eventos é a fonte autorizada de informações, é possível excluir as visualizações materializadas e reproduzir todos os eventos passados para criar uma nova representação do estado atual quando o sistema evolui ou quando o modelo de leitura deve mudar.
		- As visualizações materializadas são efetivamente um cache durável de somente leitura dos dados.
		- Consistencia eventual (melhor que no 2 modelo)
		- Alto nivel de complexidade
		- requer a reaprendizagem de alguns conceitos e uma abordagem diferente para projetar sistemas
		- No entanto, o Event Sourcing pode facilitar a modelagem do domínio e a reconstrução de visualizações ou a criação de novas, porque a intenção das alterações nos dados é preservada
	- Quando o event source e usado para aplimentar as fontes de comands
		- Viabilizada a alimentacao de sistemas com base de dados autonomas
		- Integracao de sistemas 
		- Permite analise de dados em tempo real, sem a necessidade de processos secundarios em 2 plano que ocorram "na madrugada" 
		- A geração de visualizações materializadas para uso no modelo de leitura ou projeções dos dados, reproduzindo e manipulando os eventos para entidades específicas ou coleções de entidades, pode exigir tempo de processamento e uso de recursos consideráveis, especialmente se exigir soma ou análise de valores por longos períodos, porque todos os eventos associados podem precisar ser examinados. Isso pode ser parcialmente resolvido através da implementação de instantâneos dos dados em intervalos agendados, como uma contagem total do número de uma ação específica que ocorreu ou o estado atual de uma entidade.

---


Graph dbs