Roteiro
---

1. Preparar
   - Deletar topico `tb.class` 
   - Deletar topico `tp.student` 
   - Criar topico `tb.class` (tb, retention -1)
   - Criar topico `tb.grade` (tb, retention -1)
   - Criar topico `tp.student` (stream)
   - Criar topico `tp.student.grade` (stream)
   - Limpar mongo `class`
   - Limpar mongo `enrollment`
   - Limpar postgre `student`
   - Limpar postgre `grade`
   - Limpar elasticsearch
   - Limpar redis
2. Cadastrar turma
   - Cadastro de x turmas
   - Verificar dados no table do kafka (tb.class)
   - Verificar dados no mongo (class)
   - Verificar dados no redis (CLASS:01)
3. Cadastrar aluno
   - Cadastro de x alunos
   - Verificar no postgre (student)
   - Verificar no topico (tp.student)
   - Matricula ira cadastrar a Matricula de aluno em turma
   - Verificar matricula no mongo (enrollment)
4. Cadastro da nota
   - Cadastro de x notas
   - Verificar notas no kafka (tb.grade)
   - Cadastro da nota no postgres
   - Verificar nota no postgres (grade)
   - Verificar no kafka o join do matricula de aluno com nota
     (tp.student.grade)
   - Armazenar dado da Matricula do aluno com a nota no elasticsearch
   - Verificar armazenamento dos dados no elasticsearch
