KAFKA
---

Elementos

- zookeeper
- kafka
- schema-registry
- connect
- control-center
- ksql-server
- ksql-cli
- ksql-datagen
- rest-proxy

- mongodb
- postgresql
- redis
- elasticsearch


---

base de dados 2: Dados de escolas e turmas
- Mongodb

base de dados 1: Base de dados de alunos 
- Postgre

base de dados 3: Base de notas
- Postgre

base de dados 4: Notas de alunos
- Elasticsearch


- ms 3
  -   cadastra escolas e turmas (async)
      
- ms 1
  -   cadastra alunos (sincrono)
  
- ms 2
  -   cadastra notas
        
- ms 4
  -   leitura de dados de alunos
  
- ms 5
  -   leitura de dados de escolas e turmas
  
- ms 6
  -   leitura de dados de notas
  
- ms 7
  -   leitura de dados de notas de alunos
  
---

- Cadastrar turma (async)
    - KTable
    - Armazenar no mongodb
   -   O conector de redis ira escutar o topico e armazenar o dado no
       redis
- Cadastrar aluno (sync)
  -   Armazenar no postgre
  -   Ao cadastrar aluno no postgres (sincrona), evento de cadastro
      (debezium) sera enviado para topico (kstream)
  - Evento sera escutado pelo servico de matricula e armazenado no
       mongodb
- Cadastrar nota
    -   KTable
    -   Evento de nota
    -     Cadastro de nota no postgres
    -     Join de nota com enturmacao de aluno
    -     Armazena dado de nota + enturmacao de aluno no Elasticsearch
- Fazer leitura da nota do aluno com dados de aluno, turma, escola e
  nota no elasticsearch



--- 

disciplina
(
id
curso
)

turma - cadastrar e join com disciplina para trazer os dados no disciplina - stream de disciplina para atualizar esta base
(
id
id_disciplina
)

aluno
(
id
)

matricula
(
id
id_aluno
id_turma
)

comunicacao
(

)



visao do adm

visao do aluno ; tutor
---



