package repository

import (
	"log"
	"rezpablo/event_source/projects/student/command/config"
	"rezpablo/event_source/projects/student/model"
)

type StudentRepository struct {
	PostgresDS config.PostgresDS
}

func (s *StudentRepository) Insert(student model.Student) error {

	sqlStatement := ` INSERT INTO student (name, period, cpf) VALUES ($1, $2, $3) `
	_, err := s.PostgresDS.Db.Exec(sqlStatement, student.Name, student.Period, student.Cpf)
	if err != nil {
		log.Printf("Erro ao inserir estudante: [%v]", student)
	}
	log.Printf("estudante inserido: [%v]", student)

	return err
}
