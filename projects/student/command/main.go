package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"rezpablo/event_source/projects/student/command/config"
	"rezpablo/event_source/projects/student/command/repository"
	"rezpablo/event_source/projects/student/model"
)

var postgresDS = config.PostgresDS{}
var studentRepository = repository.StudentRepository{}

func init() {
	postgresDS.Connect()
	studentRepository.PostgresDS = postgresDS
}

func main() {
	log.Printf("Inicinado ...")

	ginEngine := gin.Default()

	ginEngine.POST("/student/command", func(ctx *gin.Context) {
		httpHandle(ctx)
	})

	ginEngine.Run(fmt.Sprintf(":%v", 8091))

	defer postgresDS.Db.Close()

}

func httpHandle(ctx *gin.Context) {

	var student model.Student
	statusCode := http.StatusCreated
	result := "Estudante cadastrado..."

	if ctx.ShouldBind(&student) == nil {
		err := studentRepository.Insert(student)

		if err != nil {
			statusCode = http.StatusUnprocessableEntity
			result = err.Error()
		}

		log.Printf("%v", student)

	} else {
		statusCode = http.StatusInternalServerError
		result = "Erro geral..."
	}

	ctx.Header("Content-Type", "application/json")
	ctx.JSON(statusCode, gin.H{
		"message": result,
	})

}
