 go mod init rezpablo/event_source/projects/student
 
 ```shell script
 curl -X POST \
   http://localhost:8091/student/command \
   -H 'cache-control: no-cache' \
   -H 'content-type: application/json' \
   -H 'postman-token: c959769b-4468-ea48-79f4-5f36171ccce9' \
   -d '{
 	"name": "6 aluno",
 	"cpf": "12336985215",
 	"period": 2
 }'
 ```

# Salvar no postgre (sync)
# Configurar debezium para escutar os cadastros
# Escutar os eventos para levalos para o atualiza-los no mongo como matricula