package model

type Student struct {
	Id     int    `json:"id"`
	Name   string `json:"name"`
	Cpf    string `json:"cpf"`
	Period int    `json:"period"`
}
