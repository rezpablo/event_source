package repository

import (
	"context"
	"log"
	"rezpablo/event_source/projects/class/enrollment-command/config"
	"rezpablo/event_source/projects/class/model"
	"time"
)

const COLLECTION = "entollment"


type EnrollmentRepository struct {
	MongoDBDS config.MongoDBDS
}

func (cr *EnrollmentRepository) Insert(enrollment model.Enrollment) {

	log.Printf("%v", enrollment)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	db := cr.MongoDBDS.MgoDatabase
	collection := db.Collection(COLLECTION)

	_, err := collection.InsertOne(ctx, enrollment)

	if err != nil {
		log.Printf("%v", err)
	}

}
