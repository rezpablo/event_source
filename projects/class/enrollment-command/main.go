package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"rezpablo/event_source/projects/class/enrollment-command/config"
	"rezpablo/event_source/projects/class/enrollment-command/repository"
	"rezpablo/event_source/projects/class/kafka"
	"rezpablo/event_source/projects/class/model"
)

type EnrollmentConsumerHandler struct{}

func (EnrollmentConsumerHandler) Setup(_ sarama.ConsumerGroupSession) error   { return nil }
func (EnrollmentConsumerHandler) Cleanup(_ sarama.ConsumerGroupSession) error { return nil }

const topic = "tp.enrollment"

var mongoDBDS = config.MongoDBDS{}
var enrollmentRepository = repository.EnrollmentRepository{}
var group sarama.ConsumerGroup

func init() {
	mongoDBDS.Connect()
	enrollmentRepository.MongoDBDS = mongoDBDS
}

func main() {

	group = kafka.KafkaConsumer("enrollment.command.client", "enrollment.command.group")

	startConsumer()

}

func startConsumer() {
	defer func() { _ = group.Close() }()

	go func() {
		for err := range group.Errors() {
			fmt.Println("ERROR", err)
		}
	}()

	ctx := context.Background()
	for {
		topics := []string{topic}
		handler := &EnrollmentConsumerHandler{}

		err := group.Consume(ctx, topics, handler)
		if err != nil {
			panic(err)
		}

	}
}

func (h EnrollmentConsumerHandler) ConsumeClaim(sess sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		fmt.Printf("Message topic:%q partition:%d offset:%d\n", msg.Topic, msg.Partition, msg.Offset)

		enrollment := model.Enrollment{}
		err := json.Unmarshal(msg.Value, &enrollment)

		if err == nil {
			enrollmentRepository.Insert(enrollment)
			sess.MarkMessage(msg, "")
		}

	}
	return nil
}
