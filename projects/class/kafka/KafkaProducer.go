package kafka

import (
	"encoding/json"
	"github.com/Shopify/sarama"
	"log"
)

const broker = "localhost:9092"

type KafkaProducer struct {
	Producer sarama.SyncProducer
}

func Producer(clientId string) (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.Retry.Max = 10
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.MaxMessageBytes = 15000000
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	config.ClientID = clientId
	config.Version = sarama.V0_11_0_0

	producer, err := sarama.NewSyncProducer([]string{broker}, config)

	if err != nil {
		log.Panic("Error Create producer: ", err.Error())
	}

	return producer, err
}

func (p *KafkaProducer) Send(data interface{}, key string, topic string) {

	dataJson, err := json.Marshal(data)

	if err != nil {
		log.Printf("Error publish: %s", err.Error())
	}

	msg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(dataJson),
		Key:   sarama.StringEncoder(key),
	}
	_, _, err = p.Producer.SendMessage(msg)

	if err != nil {
		log.Printf("Error publish: %s", err.Error())
	}

}
