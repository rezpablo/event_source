package kafka

import (
	"github.com/Shopify/sarama"
	"log"
	"time"
)

func Topic(topic string, table bool) {
	broker := sarama.NewBroker("localhost:9092")
	config := sarama.NewConfig()
	config.Version = sarama.V1_0_0_0
	broker.Open(config)

	connected, err := broker.Connected()
	if err != nil {
		log.Print(err.Error())
	}
	log.Print(connected)

	topicDetail := &sarama.TopicDetail{}
	topicDetail.NumPartitions = int32(1)
	topicDetail.ReplicationFactor = int16(1)
	topicDetail.ConfigEntries = make(map[string]*string)

	if table {
		retention := "-1"
		policy := "compact"
		topicDetail.ConfigEntries["retention.ms"] = &retention
		topicDetail.ConfigEntries["cleanup.policy"] = &policy
	}

	topicDetails := make(map[string]*sarama.TopicDetail)
	topicDetails[topic] = topicDetail

	request := sarama.CreateTopicsRequest{
		Timeout:      time.Second * 15,
		TopicDetails: topicDetails,
	}

	response, err := broker.CreateTopics(&request)

	if err != nil {
		log.Printf("%#v", &err)
	}

	log.Printf("the response is %#v", response)

	broker.Close()
}
