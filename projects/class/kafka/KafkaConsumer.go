package kafka

import (
	"github.com/Shopify/sarama"
)

func KafkaConsumer(clientId string, groupId string) sarama.ConsumerGroup {

	config := sarama.NewConfig()
	config.Version = sarama.V2_4_0_0
	config.Consumer.Return.Errors = true
	config.ClientID = clientId
	brokers := []string{"localhost:9092"}

	client, err := sarama.NewClient(brokers, config)
	if err != nil {
		panic(err)
	}

	group, err := sarama.NewConsumerGroupFromClient(groupId, client)

	return group
}
