package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"rezpablo/event_source/projects/class/kafka"
	"rezpablo/event_source/projects/class/model"
	"strconv"
)

var kafkaProducer = kafka.KafkaProducer{}

const topic = "tb.class"

func main() {
	log.Printf("Inicinado ...")

	//kafka.Topic(topic, true)

	kafkaSyncProducer, _ := kafka.Producer("class.command.producer")
	kafkaProducer.Producer = kafkaSyncProducer

	r := gin.Default()

	r.POST("/class/proxy-command", func(c *gin.Context) {
		httpHandle(c)
	})

	r.Run(fmt.Sprintf(":%v", 8090))

	defer kafkaSyncProducer.Close()
}

func httpHandle(c *gin.Context) {

	var class model.Class
	statusCode := http.StatusOK
	result := "Cadastro iniciado..."

	if c.ShouldBind(&class) == nil {
		log.Printf("%v", class)
		kafkaProducer.Send(class, strconv.Itoa(class.Period), topic)

	} else {
		statusCode = http.StatusInternalServerError
		result = "Erro geral..."
	}

	c.Header("Content-Type", "application/json")
	c.JSON(statusCode, gin.H{
		"message": result,
	})

}
