module rezpablo/event_source/projects/class

go 1.13

require (
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/Shopify/sarama v1.26.0
	github.com/gin-gonic/gin v1.5.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.mongodb.org/mongo-driver v1.2.1
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
)
