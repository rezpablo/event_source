package repository

import (
	"context"
	"log"
	"rezpablo/event_source/projects/class/command/config"
	"rezpablo/event_source/projects/class/model"
	"time"
)

const COLLECTION = "class"


type ClassRepository struct {
	MongoDBDS config.MongoDBDS
}

func (cr *ClassRepository) Insert(class model.Class) {

	log.Printf("%v", class)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	db := cr.MongoDBDS.MgoDatabase
	collection := db.Collection(COLLECTION)

	_, err := collection.InsertOne(ctx, class)

	if err != nil {
		log.Printf("%v", err)
	}

}
