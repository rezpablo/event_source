package config

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	connectionString = "mongodb://localhost:27017"
	collection       = "class"
)

type MongoDBDS struct {
	MgoDatabase *mongo.Database
}

func (ds *MongoDBDS) Connect() {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	session, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionString))

	if err != nil {
		panic(err)
	}
	ds.MgoDatabase = session.Database(collection)
}
