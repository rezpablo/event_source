package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"rezpablo/event_source/projects/class/command/config"
	"rezpablo/event_source/projects/class/command/repository"
	"rezpablo/event_source/projects/class/kafka"
	"rezpablo/event_source/projects/class/model"
)

type ClassConsumerHandler struct{}

func (ClassConsumerHandler) Setup(_ sarama.ConsumerGroupSession) error   { return nil }
func (ClassConsumerHandler) Cleanup(_ sarama.ConsumerGroupSession) error { return nil }

const topic = "tb.class"

var mongoDBDS = config.MongoDBDS{}
var classRepository = repository.ClassRepository{}
var group sarama.ConsumerGroup

func init() {
	mongoDBDS.Connect()
	classRepository.MongoDBDS = mongoDBDS

	//kafka.Topic(topic, true)
}

func main() {

	group = kafka.KafkaConsumer("class.command.client", "class.command.group")

	startConsumer()
}

func startConsumer() {
	defer func() { _ = group.Close() }()

	go func() {
		for err := range group.Errors() {
			fmt.Println("ERROR", err)
		}
	}()

	ctx := context.Background()
	for {
		topics := []string{topic}
		handler := &ClassConsumerHandler{}

		err := group.Consume(ctx, topics, handler)
		if err != nil {
			panic(err)
		}

	}
}

func (h ClassConsumerHandler) ConsumeClaim(sess sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		fmt.Printf("Message topic:%q partition:%d offset:%d\n", msg.Topic, msg.Partition, msg.Offset)

		class := model.Class{}
		err := json.Unmarshal(msg.Value, &class)

		if err == nil {
			classRepository.Insert(class)
			sess.MarkMessage(msg, "")
		}

	}
	return nil
}
