 go mod init rezpablo/event_source/projects/class

Cadastrar class


```shell script
curl -X POST \
  http://localhost:8090/class/proxy-command \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: c959769b-4468-ea48-79f4-5f36171ccce9' \
  -d '{
	"code": "CALC1",
	"name": "Calculo I",
    "course": "BSI",
    "period": 2
}'
```

```shell script
curl -X POST \
  http://localhost:8090/class/proxy-command \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: c959769b-4468-ea48-79f4-5f36171ccce9' \
  -d '{
	"code": "CALC2",
	"name": "Calculo II",
    "course": "BSI",
    "period": 3
}'
```




# Configurar connector de redis, que ira escutar o topico e atualizar/criar o cache   
# Salvar no mongo
# Criar matricula... escutando o topico de alunos, realizar a matricula (atraves de join) e armazenar no mongo