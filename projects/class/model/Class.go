package model

type Class struct {
	Name   string `json:"name"`
	Code   string `json:"code"`
	Type   string `json:"type"`
	Period int    `json:"period"`
}
