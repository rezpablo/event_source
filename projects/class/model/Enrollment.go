package model

type Enrollment struct {
	Student Student `json:"student"`
	Class   Class   `json:"class"`
}

type Student struct {
	Id     int    `json:"id"`
	Name   string `json:"name"`
	Cpf    string `json:"cpf"`
	Period int    `json:"period"`
}
