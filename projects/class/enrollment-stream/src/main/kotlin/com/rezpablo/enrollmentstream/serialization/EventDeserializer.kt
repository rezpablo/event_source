package com.rezpablo.enrollmentstream.serialization

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.serialization.Deserializer
import java.io.IOException
import java.nio.charset.Charset
import java.util.*

class EventDeserializer : Deserializer<Map<String, Any>> {

    override fun deserialize(topic: String?, data: ByteArray?): Map<String, Any> {
        if (data == null || data.size == 0) {
            return HashMap()
        }

        var result: Map<String, Any> = HashMap()

        try {
            result = ObjectMapper().readValue(String(data, Charset.forName("UTF-8")), Map::class.java ) as Map<String, Any>
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    override fun close() {}
    override fun configure(configs: MutableMap<String, *>?, isKey: Boolean) {}

}
