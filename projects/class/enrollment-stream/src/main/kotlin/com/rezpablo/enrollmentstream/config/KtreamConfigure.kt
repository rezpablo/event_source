package com.rezpablo.enrollmentstream.config

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.StreamsConfig
import java.util.*

class KtreamConfigure {


    fun configure() : Properties {
        val bootstrapServersConfig = System.getenv("BOOTSTRAP_SERVERS_CONFIG") ?: "localhost:9092"
        val applicationIdConfig = System.getenv("APPLICATION_ID_CONFIG") ?: "kafka-enrollment-stream"
        val props = Properties()

        with(props) {
            put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig)
            put(StreamsConfig.APPLICATION_ID_CONFIG, applicationIdConfig) // + UUID.randomUUID().toString()
            put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().javaClass.name) // org.apache.kafka.common.serialization.Serdes$StringSerde
            put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, "com.rezpablo.enrollmentstream.serialization.EventSerdes")
            put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
        }

        return props
    }

}
