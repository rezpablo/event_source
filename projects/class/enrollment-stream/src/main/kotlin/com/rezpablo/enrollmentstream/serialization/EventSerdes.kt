package com.rezpablo.enrollmentstream.serialization

import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serializer

class EventSerdes : Serde<Map<String, Any>> {

    override fun configure(configs: MutableMap<String, *>?, isKey: Boolean) {}
    override fun close() {}
    override fun deserializer(): Deserializer<Map<String, Any>>? = EventDeserializer()
    override fun serializer(): Serializer<Map<String, Any>> = EventSerializer()

}
