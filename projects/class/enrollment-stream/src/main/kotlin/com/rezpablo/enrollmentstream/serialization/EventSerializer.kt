package com.rezpablo.enrollmentstream.serialization

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.serialization.Serializer
import org.apache.kafka.common.serialization.StringSerializer
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream

class EventSerializer : Serializer<Map<String, Any>> {

    override fun serialize(topic: String?, data: Map<String, Any>?): ByteArray {

//        val baos : ByteArrayOutputStream = ByteArrayOutputStream(1024);
//        val oos : ObjectOutputStream = ObjectOutputStream(baos)
//        oos.writeObject(data);
//        oos.flush();
//        return baos.toByteArray();

        return ObjectMapper().writeValueAsString(data).toString().toByteArray()

    }

    override fun close() {}
    override fun configure(configs: MutableMap<String, *>?, isKey: Boolean) {}
}
