package com.rezpablo.enrollmentstream.stream

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.kstream.GlobalKTable
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Predicate
import java.nio.charset.Charset
import java.util.*


class StreamsProcessor {

    fun process(props: Properties) {
        // json Serde
//        val jsonSerializer = JsonSerializer()
//        val jsonDeserializer = JsonDeserializer()
//        val jsonSerde = Serdes.serdeFrom<JsonNode>(jsonSerializer, jsonDeserializer)

        val builder = StreamsBuilder()

        val nodeTable: GlobalKTable<String, Map<String, Any>> = builder.globalTable("tb.class") // , Consumed.with(Serdes.String(), jsonSerde)
        val nodeStreams: KStream<String, Map<String, Any>> = builder.stream("dbserver1.public.student") // , Consumed.with(Serdes.String(), jsonSerde)
        val localStream:  KStream<String, Map<String, Any>> = nodeStreams.selectKey { key, value ->((value.get("payload") as Map<String,Any>)?.get("after") as Map<String, Any>)?.get("period")?.toString() };
        this.joiStreams(localStream, nodeTable)

        val streams = KafkaStreams(builder.build(), props)
        streams.cleanUp()
        streams.start()

        streams.localThreadsMetadata().forEach { data -> println(data) }

        Runtime.getRuntime().addShutdownHook(Thread(streams::close))
    }

    private fun joiStreams(nodeStreams: KStream<String, Map<String, Any>>, nodeTable: GlobalKTable<String, Map<String, Any>>) {

        nodeStreams       // a key foi tratada no stream -- por isto substituir globalKtable por ktable
                .leftJoin(nodeTable, { key, value ->((value.get("payload") as Map<String,Any>)?.get("after") as Map<String, Any>)?.get("period")?.toString() },  { v1, v2 -> mapOf("class" to v2, "student" to (v1.get("payload") as Map<String,Any>)?.get("after")) })
                .to("tp.enrollment")
//                .foreach { key, value -> println("VALUES: $value") }
    }

}
