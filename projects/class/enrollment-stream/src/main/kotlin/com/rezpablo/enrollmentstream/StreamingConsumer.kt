package com.rezpablo.enrollmentstream

import com.rezpablo.enrollmentstream.config.KtreamConfigure
import com.rezpablo.enrollmentstream.stream.StreamsProcessor

fun main(args: Array<String>) {

    val ktreamConfigure = KtreamConfigure()
    val props = ktreamConfigure.configure()

    val stream = StreamsProcessor()
    stream.process(props)

}
