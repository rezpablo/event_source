#!/bin/bash

docker rm -f postgres
docker run -d -it --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=student -p 5432:5432 postgres
