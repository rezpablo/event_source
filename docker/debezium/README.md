

https://github.com/debezium/debezium-examples/blob/master/tutorial/docker-compose-postgres.yaml

https://github.com/debezium/debezium-examples/blob/master/tutorial/README.md#using-postgres


# Start the topology as defined in http://debezium.io/docs/tutorial/
export DEBEZIUM_VERSION=0.10
docker-compose -f docker-compose-postgres.yaml up

# Start Postgres connector
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-postgres.json
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-postgres-default.json

# Consume messages from a Debezium topic
docker-compose -f docker-compose-postgres.yaml exec kafka /kafka/bin/kafka-console-consumer.sh \
    --bootstrap-server kafka:9092 \
    --from-beginning \
    --property print.key=true \
    --topic dbserver1.inventory.customers

# Modify records in the database via Postgres client
docker-compose -f docker-compose-postgres.yaml exec postgres env PGOPTIONS="--search_path=inventory" bash -c 'psql -U $POSTGRES_USER postgres'
docker-compose exec postgres env PGOPTIONS="--search_path=inventory" bash -c 'psql -U $POSTGRES_USER postgres'

# Shut down the cluster
docker-compose -f docker-compose-postgres.yaml down


--


https://debezium.io/documentation/reference/1.0/connectors/postgresql.html
https://debezium.io/documentation/reference/1.0/connectors/postgresql.html#replica-identity